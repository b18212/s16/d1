let faveFood = "spaghetti";
console.log(faveFood);

let isPresent = true;
console.log(isPresent);

let person = {
	fullName: "Hannah Faye",
	lastName: "Ordonio",
	stageName: "Hannah",
	birthDay: "03/09/1991",
	age: 30,
	bestAlbum: "Proof",
	bestTVShow: null,
	bestSong: "King",
	bestMovie: null,
	isActive: true
}

let restaurants = ["McDonalds", "Jollibee", "Chowking", "KFC", "Mang Inasal"];

console.log(restaurants);
console.log(person);


//Operators

// Arithmetic Operators
	
	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);
	// result: 9228

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);
	// result: -6434

	let product = x * y;
	console.log("Result of multiplication operator: " + product);
	// result: 10939907

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);
	// result: 0.17839356404035245

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);
	// result: 846


	// Assignment Operator
	// Basic Assignment Operator (=)

	let assignmentNumber = 8;
	 //  Shorthand arithmetic Operators

	 // Addition Assignment Operator (+=)

	 assignmentNumber = assignmentNumber + 2;
	 console.log("Result of additon assingnment: " + assignmentNumber);

	 assignmentNumber += 2;
	 console.log("Result of additon assingnment: " + assignmentNumber);
	 
	 let string1 = "Boston";
	 let string2 = "Celtics";

	 string1 += string2;
	 console.log(string1);

	 assignmentNumber -= 2;
	 console.log("Result of subtraction assignment operator: " + assignmentNumber);

	 string1 -= string2
	 console.log(string1);

	 assignmentNumber *= 2;
	 console.log("Result of multiplication assignment operator: " + assignmentNumber);

	 assignmentNumber /= 2;
	 console.log("Result of division assignment operator: " + assignmentNumber);

	 // Multiple Operators and Parentheses

	 	let mdas = 1 + 2 - 3 * 4 / 5
	 	console.log("Result of mdas: " + mdas);

	 	/* 
	 		1. 3 * 4 = 12
	 		2. 12 / 5 = 2.4
	 		3. 1 + 2 = 3
	 		4. 3 - 2.4 = 0.6

	 	*/


	 	let pemdas =  1 + (2 - 3) * (4 / 5);
	 	console.log("Result of pemdas: " + pemdas);

	 	/*
	 		1. 4 / 5 = 0.8
	 		2. 2 - 3 = -1
	 		3. -1 * 0.8 = -0.8
	 		4. 1 + -0.8 = 0.2
	 	*/

	 	// Increment and Decrement

	 		// pre-increment and post-increment

	 		let z = 1;

	 		let increment = ++z;
	 		console.log("Result of pre-increment: " + increment);
	 		console.log("Result of pre-increment: " + z);

	 		increment = z++;
	 		console.log("Result of post-increment: " + increment);
	 		console.log("Result of post-increment: " + z);

	 	let decrement = --z;
	 	console.log("Result of pre-decrement: " + decrement);
	 	console.log("Result of pre-decrement: " + z);

	 	decrement = z--;
	 	console.log("Result of post-decrement: " + decrement);
	 	console.log("Result of post-decrement: " + z);


	 	// Type Coercion
	 	let numA = "10";
	 	let numB = 12;

	 	let coercion = numA + numB;
	 	console.log("Result of coercion: " + coercion);
	 	console.log(typeof coercion);

	 	let numC = 16;
	 	let numD = 14;
	 	let nonCoercion = numC + numD;
	 	console.log("Non-coercion: " + nonCoercion);
	 	console.log(typeof nonCoercion);

	 	let numE =  true + 1;
	 	// true === 1
	 	// 1 + 1 = 2
	 	console.log(numE);

	 	let numF = false + 1;
	 	// false === 0
	 	//  0 + 1 = 1
	 	console.log(numF);


// Comparison Operators
	// Equality Operator (==)

	let anya = "anya";
	console.log("anya" == "anya");
	console.log("anya" == anya);
	console.log(1 == false);
	console.log(1 == true);
	console.log("false" == false); /* cannot be converted the string to boolean


/* Loose Inequality Operator (!=) */
console.log(1 != 1);

console.log("1" != 1);
console.log(anya != "anya");
console.log(1 != 2);


// Strict Equality Operator (===)
console.log(1 === 1);
console.log("1" === 1);
console.log(0 === false);

// Strict Inequality Operator (!==)
console.log("1" !== true);
console.log("Anya" !== "anya"); // case sensitive

// Relational Operators

let a = 50;
let b = 65;

// GT or greater than operator (>)
let isGreaterThan = a > b
console.log(isGreaterThan);

// LT or less than operator (<)
let isLessThan = a < b;
console.log(isLessThan);


// GTE and LTE or Greater than or Equal and Less than or Equal (>= or <=)
let isGTE = a >= b;
let isLTE = a <= b;

console.log(isGTE, isLTE);

let numStr = "30";
console.log(a > numStr); // forced type coercion to change the string to number

let str = "twenty";
console.log (b >= str);

// Logical Operators

	// let isLegalAge = true;
	// let isRegistered = false;

	// AND Operator (&& -  double ampersand)
		// returns true if ALL operands are true

	// let allRequirementsMet = isLegalAge && isRegistered;
	// console.log(allRequirementsMet);



	// OR Operator (|| - double pipe)
		// Returns true IF one of the operands are true

	let isLegalAge = true;
	let isRegistered = false;

	let someRequirementsMet = isLegalAge || isRegistered;
	console.log(someRequirementsMet);

	// NOT Operator (!)
		// turns a boolean into opposite value
	let someRequirementsNotMet = !isRegistered;
	console.log(someRequirementsNotMet);

	// Mulitiple Operators

	let requiredLevel = 95
	let requiredAge = 18

	let authorization1 = isRegistered && requiredLevel === 25;
	console.log(authorization1);


	let authorization2 = isRegistered && isLegalAge && requiredLevel === 95;
	console.log(authorization2);

	let authorization3 = !isRegistered && isLegalAge && requiredLevel === 95;
	console.log(authorization3);
